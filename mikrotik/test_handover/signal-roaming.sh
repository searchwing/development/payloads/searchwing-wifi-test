#!/bin/bash
TEST_DIRECTORY="${1}"

for i in {1..99999}
do
  ssh admin@192.168.42.1 /interface/wireless registration-table print stats > ${TEST_DIRECTORY}/$(date +"%Y-%m-%dT%H-%M-%S-%N")_signal
  sleep 3
done 
