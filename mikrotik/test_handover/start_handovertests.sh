#!/bin/bash

TEST_DIRECTORY="${1}"

mkdir -p ${TEST_DIRECTORY}

ssh admin@192.168.42.1 "/interface/wireless print advanced" > ${TEST_DIRECTORY}/configuration_ap
ssh admin@192.168.42.2 "/interface wireless print advanced" > ${TEST_DIRECTORY}/configuration_sta

bash speedtest.sh ${TEST_DIRECTORY} &
bash signal-roaming.sh ${TEST_DIRECTORY}
