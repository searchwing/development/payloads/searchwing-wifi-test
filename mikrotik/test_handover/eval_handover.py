import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from glob import glob
import os
import re
import datetime
import time

import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="analyse wfb data.")
    parser.add_argument("-i",
                        "--inputFolder",
                        help="", type=str, default="/home/julle/projects/SearchWing/Tests/20220513_wifiTestRauen/20220513_integrationTest")
    args = parser.parse_args()

    args.pcap = True

    #USER SETUP
    path=args.inputFolder

    while True:

        #path to test results
        testfiles = sorted(glob(path+"/*"))[-100:]
        testPositionsDirNames = [os.path.basename(os.path.normpath(path)) for path in testfiles]

        df_signals=pd.DataFrame([],columns=['resultDatetime','interface','signalDbm','txsignalDbm'])
        df_speedtests=pd.DataFrame([],columns=['resultDatetime','speed'])

        #read in raw result data
        for idxTestPos,oneTestPosPath in enumerate(testfiles):
            if "png" in oneTestPosPath:
                continue
            try:
                resultDatetime=datetime.datetime.strptime(os.path.basename(oneTestPosPath)[0:19], '%Y-%m-%dT%H-%M-%S') #datetime.datetime(2022, 8, 13, 7, 1, 16,0, pytz.UTC)
                print("Parse test pos "+oneTestPosPath + "...")
                if "signal" in oneTestPosPath:
                    # signal
                    with open(oneTestPosPath) as f:
                        lines = f.readlines()
                        #stamp_string=os.path.basename(oneTestPosPath)[0:-7]+"-10000"
                        #stamp=datetime.datetime.strptime(stamp_string,"%Y-%m-%dT%H-%M-%S-%N")
                        signalDbm=0
                        txsignalDbm=0
                        for line in lines:
                            data = re.findall(r"interface=[^ ]+", line)
                            if len(data) > 0:
                                interface=data[0].split("=")[1]
                            data = re.findall(r"signal-strength=[+-]?([0-9]+([.]?[0-9]*))", line)
                            if len(data) > 0:
                                val = float(data[0][0])
                                signalDbm=val
                            data = re.findall(r"tx-signal-strength=[+-]?([0-9]+([.]?[0-9]*))", line)
                            if len(data) > 0:
                                val = float(data[0][0])
                                txsignalDbm=val
                            data = re.findall(r"bytes=[+-]?([0-9]+([.]?[0-9]*)),[+-]?([0-9]+([.]?[0-9]*))", line)
                            if len(data) > 0:
                                val = float(data[0][0])
                                bytes0=val
                                val = float(data[0][2])
                                bytes1=val


                    df_signals=df_signals.append(
                        {"interface":interface,
                        "signalDbmSide":0 if interface == "forward" else signalDbm,
                        "signalDbmFront":0 if interface == "side" else signalDbm,
                        "signalDbm":signalDbm,
                        "txsignalDbm":txsignalDbm,
                        "resultDatetime":resultDatetime,
                        },ignore_index=True)            
                if "speed" in oneTestPosPath:
                    # speed
                    speed=0
                    with open(oneTestPosPath) as f:
                        lines = f.readlines()
                        valid=False
                        for line in lines:
                            if "done testing" in line:
                                valid=True
                            if valid == True:
                                data = re.findall(r"rx-total-average: [+-]?([0-9]+([.]?[0-9]*))", line)
                                if len(data) > 0:
                                    val = float(data[0][0])
                                    if "kbps" in line:
                                        val = val / 1e3
                                    speed=val#*10

                    df_speedtests=df_speedtests.append(
                        {
                        "speed":speed,
                        "resultDatetime":resultDatetime,
                        },ignore_index=True)
            except:
                continue    


        #plot
        try:
            plt.figure(1)
            fig, ax1 = plt.subplots()
            ax2 = ax1.twinx()
            ax1.scatter(df_signals['resultDatetime'], df_signals['signalDbmSide'].values,label='signalSide[dBm]',color='red')
            ax1.scatter(df_signals['resultDatetime'], df_signals['signalDbmFront'].values,label='signalFront[dBm]',color='blue')
            ax2.scatter(df_speedtests['resultDatetime'], df_speedtests['speed'].values,label='speed[Mbps]',color='black')
            ax1.set_xlabel('time')
            ax1.set_ylabel('dbm')
            ax2.set_ylabel('speed in mbit/s')
            ax1.legend(loc='center left')
            ax2.legend(loc='center right')
            plt.title('data')
            plt.xlabel("Datetime")
            plt.xticks(rotation=90)
            plt.legend(loc='lower left')
            plt.grid()
            patha = os.path.join(path,'_signallevels_static.png')
            print(patha)
            plt.savefig(patha,facecolor="w",dpi=150, bbox_inches='tight')
            plt.close()

            
        except:
            pass

        time.sleep(2)