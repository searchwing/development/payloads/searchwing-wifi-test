#!/bin/bash

TEST_DIRECTORY=${1}

for i in {1..99999}
do
  ssh admin@192.168.42.1 tool/bandwidth-test address=192.168.42.2 duration=5 direction=receive protocol=tcp > ${TEST_DIRECTORY}/$(date +"%Y-%m-%dT%H-%M-%S-%N")_speed
  #sleep 1
done
