TEST_DIRECTORY="${1}"
TEST_DURATION="60"
let TEST_DURATION_ALL=$TEST_DURATION*5

mkdir ${TEST_DIRECTORY}

#ssh admin@192.168.42.1 "/interface/wireless disable wlan1"
ssh admin@192.168.42.1 "/interface/wireless enable wlan1"
sleep 30 # wait for wifi to start
ssh admin@192.168.42.1 "/interface/wireless registration-table print stats"
ssh admin@192.168.42.1 "/interface/wireless print advanced" > ${TEST_DIRECTORY}/configuration_ap
ssh admin@192.168.42.1 "/interface print stats" > ${TEST_DIRECTORY}/stats_start
timeout $TEST_DURATION_ALL ./signal.sh > ${TEST_DIRECTORY}/signal &
ssh admin@192.168.42.1 "tool/speed-test address=192.168.42.2 test-duration=${TEST_DURATION}"| tee ${TEST_DIRECTORY}/speed-test
ssh admin@192.168.42.1 "/interface print stats" > ${TEST_DIRECTORY}/stats_end
ssh admin@192.168.42.1 "/log print" > ${TEST_DIRECTORY}/log
