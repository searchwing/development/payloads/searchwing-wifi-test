import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from glob import glob
import os
import re
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="analyse wfb data.")
    parser.add_argument("-i",
                        "--inputFolder",
                        help="", type=str, default="/home/julle/projects/SearchWing/Tests/20220513_wifiTestRauen/20220513_integrationTest")
    args = parser.parse_args()

    args.pcap = True

    #USER SETUP
    path=args.inputFolder

    #path to test results
    testPositions = sorted(glob(path+"/*/"))
    testPositionsDirNames = [os.path.basename(os.path.normpath(path)) for path in testPositions]

    df_results=pd.DataFrame([],columns=['testname','signalDbm','txsignalDbm','udpTx','udpRx','tcpTx','tcpRx','lossRate'])

    #read in raw result data
    for idxTestPos,oneTestPosPath in enumerate(testPositions):
        print("Parse test pos "+oneTestPosPath + "...")
        testName = testPositionsDirNames[idxTestPos]
        # speedtest
        speedtestfilename = os.path.join(oneTestPosPath,"speed-test")
        with open(speedtestfilename) as f:
            lines = f.readlines()
            udpTx=[] 
            udpRx=[]
            tcpTx=[]
            tcpRx=[]
            lossRate=[]
            for line in lines:
                data = re.findall(r"loss: ([0-9]+([.]?[0-9]*))%", line)
                if len(data) > 0:
                    val = float(data[0][0])
                    lossRate.append(val)
                data = re.findall(r"tcp-download: ([0-9]+([.]?[0-9]*))", line)
                if len(data) > 0:
                    val = float(data[0][0])
                    if "Kbps" in line:
                        val = val / 1e3
                    tcpRx.append(val)
                data = re.findall(r"tcp-upload: ([0-9]+([.]?[0-9]*))", line)
                if len(data) > 0:
                    val = float(data[0][0])
                    if "Kbps" in line:
                        val = val / 1e3
                    tcpTx.append(val)            
                data = re.findall(r"udp-download: ([0-9]+([.]?[0-9]*))", line)
                if len(data) > 0:
                    val = float(data[0][0])
                    if "Kbps" in line:
                        val = val / 1e3
                    udpRx.append(val)
                data = re.findall(r"udp-upload: ([0-9]+([.]?[0-9]*))", line)
                if len(data) > 0:
                    val = float(data[0][0])
                    if "Kbps" in line:
                        val = val / 1e3
                    udpTx.append(val)
            
            if len(lossRate)>1:
                result_lossRate=lossRate[-1]
            else:
                result_lossRate=0
            if len(udpTx)>1:
                result_udpTx=udpTx[-1]
            else:
                result_udpTx=0
            if len(udpRx)>1:
                result_udpRx=udpRx[-1]
            else:
                result_udpRx=0
            if len(tcpTx)>1:
                result_tcpTx=tcpTx[-1]
            else:
                result_tcpTx=0
            if len(tcpRx)>1:
                result_tcpRx=tcpRx[-1]
            else:
                result_tcpRx=0

        # signal
        signalfilename = os.path.join(oneTestPosPath,"signal")
        with open(signalfilename) as f:
            lines = f.readlines()
            signalDbm=[]
            txsignalDbm=[]
            for line in lines:
                data = re.findall(r"signal-strength=[+-]?([0-9]+([.]?[0-9]*))", line)
                if len(data) > 0:
                    val = -float(data[0][0])
                    signalDbm.append(val)
                data = re.findall(r"tx-signal-strength=[+-]?([0-9]+([.]?[0-9]*))", line)
                if len(data) > 0:
                    val = -float(data[0][0])
                    txsignalDbm.append(val)

        df_results=df_results.append(
            {"testname":testName,
            "lossRate":result_lossRate,
            "udpTx":result_udpTx,
            "udpRx":result_udpRx,
            "tcpTx":result_tcpTx,
            "tcpRx":result_tcpRx,
            "signalDbm":np.mean(signalDbm),
            "txsignalDbm":np.mean(txsignalDbm),
            },ignore_index=True)            
    #plot
    plt_width=15
    plt_height=10

    def plot_specific(df, colName,unit):
        plt_name = colName
        df_vis = pd.DataFrame(df[colName].values, index=df['testname'].values) 
        df_vis.plot.bar(rot=90, grid=True, figsize=(plt_width,plt_height), title=plt_name+" ["+unit+"]",fontsize=10)
        plt.tight_layout()
        patha = os.path.join(path,plt_name+'.png')
        print(patha)
        plt.savefig(patha)
        

    plot_specific(df_results,"udpTx","MBit/s")
    plot_specific(df_results,"udpRx","MBit/s")
    plot_specific(df_results,"tcpTx","MBit/s")
    plot_specific(df_results,"tcpRx","MBit/s")
    plot_specific(df_results,"lossRate","%")
    plot_specific(df_results,"signalDbm","dBm")
    plot_specific(df_results,"txsignalDbm","dBm")
