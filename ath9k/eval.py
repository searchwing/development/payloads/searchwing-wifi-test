import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from glob import glob
import os
import re
import argparse
from scapy.all import *

from helper import *

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="analyse wfb data.")
    parser.add_argument("-i",
                        "--inputFolder",
                        help="", type=str, default="/home/julle/projects/SearchWing/Tests/20220513_wifiTestRauen/20220513_integrationTest")
    parser.add_argument('--pcap', 
                        dest='pcap', 
                        action='store_true', 
                        help="Parse pcap files")
    args = parser.parse_args()

    args.pcap = True

    #USER SETUP
    path=args.inputFolder
    debugDataAvali=True
    rxAdapterCount = 1
    mode = "server"

    #path to test results
    testPositions = sorted(glob(path+"/*/"))
    testPositionsDirNames = [os.path.basename(os.path.normpath(path)) for path in testPositions]

    #read test descriptions
    testIdsPath = os.path.join(path,"testids.csv")
    df_tests = pd.read_csv(testIdsPath)
    df_tests['testid'] = pd.to_numeric(df_tests['testid'])
    for i, row in df_tests.iterrows():
        df_tests.at[i,'testdescr'] = str(i) + " " + str(row['clienttxpower']) + "dbm " + "ch"+ str(row['channel'])+ " " + "bw" +str(row['bandwidth']) + " " + "iperf " +str(row['iperf_args']) + " "  +str(row['distance'])
    testCnt = len(df_tests)
    testIdDirNames = [str(k)+"-ieee80211" for k in range(testCnt)]

    #containers
    ##csv
    df_results = {k: None for k in range(rxAdapterCount)}
    maxValuesOneAdapter = {k: [] for k in range(rxAdapterCount)}
    ##data
    testIds = []
    sizesBits = []
    testPositionsList = []
    ##debugData
    df_ieee80211=pd.DataFrame([],columns=['testid','phyNr',"testPosition","nfCal","pktsAll","crcErrorAll","validPktsRationAll","dot11FCSErrorCount","OFDM-RESTART-ERROR"])
    df_iperf=pd.DataFrame([],columns=['testid',"testPosition","sender_mbit","sender_received","sender_lost","receiver_mbit","receiver_received","receiver_lost"])
    df_pcap=pd.DataFrame([],columns=['testid',"testPosition","signals_mean","signals_median"])

    #read in raw result data
    for idxTestPos,oneTestPosPath in enumerate(testPositions):
        print("Parse test pos "+oneTestPosPath + "...")
        for idxTestId,oneTestIdPath in enumerate(testIdDirNames):
            print("Parse test id "+oneTestIdPath + "...")
            testid = int(re.findall(r"([0-9]*)-", oneTestIdPath)[0])

            if args.pcap: #and testid == 5:
                strenghts = []
                print("Parse pcap...")
                pcap_path = os.path.join(oneTestPosPath,oneTestIdPath,"searchwing-tcpdump-mon0-" + str(testid) + ".pcap")
                if os.path.exists(pcap_path):
                    command = "tcpdump -qns 0 -X -r " + pcap_path + " | grep dBm"
                    lines = subprocess.check_output(command, shell=True).splitlines()
                    for line in lines:
                        if not "192.168.1.1" in str(line):
                            continue
                        value = -float(re.findall(r"([0-9]*)dBm", str(line))[0])
                        strenghts.append(value)

                df_pcap=df_pcap.append(
                    {"testid":testid,
                    "phyNr":0,
                    "testPosition":testPositionsDirNames[idxTestPos],
                    "signals_mean":np.mean(strenghts),
                    "signals_median":np.median(strenghts)
                    },ignore_index=True)                    

            # iperf3
            iperfFileName = os.path.join(oneTestPosPath,oneTestIdPath,"iperf3-" + str(testid) + "-"+mode+".txt")
            senderMbits = []
            receiverMbits = []
            senderLosts = []
            receiverLosts = []
            senderReceived = []
            receiverReceived = []
            with open(iperfFileName) as f:
                lines = f.readlines()
                count = 0
                isUdp = False
                for line in lines:
                    if "Jitter" in line:
                        isUdp = True
                    if "sender" in line:
                        data = re.findall(r"[+-]?([0-9]+([.]?[0-9]*))\s(Mbits/sec|Kbits/sec|bits/sec)", line)
                        if len(data[0]) > 1:
                            val = float(data[0][0])
                            if "Kbits/sec" in line:
                                val = val / 1e3
                            if " bits/sec" in line:
                                val = val / 1e6
                            senderMbits.append(val)
                        if isUdp:
                            data = re.findall(r"([0-9]*)/", line)
                            if len(data) > 1:
                                senderLosts.append(float(data[1]))
                            data = re.findall(r"/([0-9]*)", line)
                            if len(data) > 1:
                                senderReceived.append(1)#float([1]))
                    if "receiver" in line:
                        data = re.findall(r"[+-]?([0-9]+([.]?[0-9]*))\s(Mbits/sec|Kbits/sec|bits/sec)", line)
                        if len(data[0]) > 1:
                            val = float(data[0][0])
                            if "Kbits/sec" in line:
                                val = val / 1e3
                            if " bits/sec" in line:
                                val = val / 1e6
                            receiverMbits.append(val)
                        if isUdp:
                            data = re.findall(r"([0-9]*)/", line)
                            if len(data) > 1:
                                data = receiverLosts.append(float(data[1]))
                            data = re.findall(r"/([0-9]*)", line)
                            if len(data) > 1:
                                data = receiverReceived.append(float(data[1]))
                            
                if not isUdp:
                    senderLosts = [0]
                    receiverLosts = [0]
                    senderReceived = [0]
                    receiverReceived = [0]

                df_iperf=df_iperf.append(
                    {"testid":testid,
                    "phyNr":0,
                    "testPosition":testPositionsDirNames[idxTestPos],
                    "sender_mbit":np.mean(senderMbits),
                    "sender_received":np.mean(senderReceived),
                    "sender_lost":np.mean(senderLosts),
                    "receiver_mbit":np.mean(receiverMbits),
                    "receiver_received":np.mean(receiverReceived),
                    "receiver_lost":np.mean(receiverLosts)
                    },ignore_index=True)
                            
        #/sys/kernel/debug/ieee80211 data
        ath9kDbgPath = "sys/kernel/debug/ieee80211/phy0/ath9k/"
        dot11Path = "sys/kernel/debug/ieee80211/phy0/statistics/"
        if debugDataAvali:
            for oneTest in df_tests.iterrows():
                testId = oneTest[1]['testid']

                for onePhyIdx in range(rxAdapterCount): 

                    pathName = os.path.join(oneTestPosPath,str(testId)+"-ieee80211")
                    startPath = os.path.join(pathName,"start")
                    
                    nfCal = -getSpecificValue(pathName,ath9kDbgPath+"dump_nfcal","start","Channel Noise Floor")
                    pktsAll=getSpecificValueDiff(pathName,ath9kDbgPath+"recv","PKTS-ALL")
                    crcErrorAll=getSpecificValueDiff(pathName,ath9kDbgPath+"recv","CRC ERR")
                    if pktsAll != 0:
                        validPktsRatioAll=(pktsAll-crcErrorAll)/pktsAll
                    else:
                        validPktsRatioAll=0
                    dot11FCSErrorCount=getSpecificValueDiff(pathName,dot11Path+"dot11FCSErrorCount","")
                    OFDMRESTARERRORS = getSpecificValueDiff(pathName,ath9kDbgPath+"phy_err","OFDM-RESTART ERR")
                    ack_to_file = open(os.path.join(pathName,"end",ath9kDbgPath,"ack_to"), 'r') 
                    ack_to_data = ack_to_file.readlines()
                    ack_to_str = re.findall(r"([0-9]*)", str(ack_to_data[0]))
                    ack_to = 0
                    if len(ack_to_str) > 1:
                        ack_to = float(ack_to_str[0])

                    df_ieee80211=df_ieee80211.append(
                        {"testid":testId,
                        "phyNr":onePhyIdx,
                        "testPosition":testPositionsDirNames[idxTestPos],
                        "nfCal":nfCal,
                        "pktsAll":pktsAll,
                        "crcErrorAll":crcErrorAll,
                        "validPktsRatioAll":validPktsRatioAll,
                        "dot11FCSErrorCount":dot11FCSErrorCount,
                        "OFDM-RESTART-ERROR":OFDMRESTARERRORS,
                        "ack_to":ack_to
                        },ignore_index=True)
                    
    #plot
    plt_width=15
    plt_height=5

    def plot_specific(df, colName, text):
        plt_name = text + " " +colName
        allTestPositons={testPositionsDirName : df[df.testPosition == testPositionsDirName][colName].values for testPositionsDirName in testPositionsDirNames}
        
        df_vis = pd.DataFrame(allTestPositons, index=df_tests['testdescr'])
        df_vis.plot.bar(rot=90, grid=True, figsize=(plt_width,plt_height), title=plt_name)
        plt.tight_layout()
        patha = os.path.join(path,plt_name+'.png')
        print(patha)
        plt.savefig(patha)
        

    for k in range(rxAdapterCount):

        plot_specific(df_iperf[df_iperf['phyNr'] == k],"sender_mbit", "Ant"+str(k)+ " dbg")
        plot_specific(df_iperf[df_iperf['phyNr'] == k],"sender_received", "Ant"+str(k)+ " dbg")
        plot_specific(df_iperf[df_iperf['phyNr'] == k],"sender_lost", "Ant"+str(k)+ " dbg")
        plot_specific(df_iperf[df_iperf['phyNr'] == k],"receiver_mbit", "Ant"+str(k)+ " dbg")
        plot_specific(df_iperf[df_iperf['phyNr'] == k],"receiver_received", "Ant"+str(k)+ " dbg")
        plot_specific(df_iperf[df_iperf['phyNr'] == k],"receiver_lost", "Ant"+str(k)+ " dbg")
        if args.pcap:
            plot_specific(df_pcap[df_pcap['phyNr'] == k],"signals_mean", "Ant"+str(k)+ " dbg")
            plot_specific(df_pcap[df_pcap['phyNr'] == k],"signals_median", "Ant"+str(k)+ " dbg")

        if debugDataAvali:
            plot_specific(df_ieee80211[df_ieee80211['phyNr'] == k],"nfCal", "Ant"+str(k)+ " dbg")
            plot_specific(df_ieee80211[df_ieee80211['phyNr'] == k],"pktsAll", "Ant"+str(k)+ " dbg")
            plot_specific(df_ieee80211[df_ieee80211['phyNr'] == k],"crcErrorAll", "Ant"+str(k)+ " dbg")
            plot_specific(df_ieee80211[df_ieee80211['phyNr'] == k],"validPktsRatioAll", "Ant"+str(k)+ " dbg")
            plot_specific(df_ieee80211[df_ieee80211['phyNr'] == k],"dot11FCSErrorCount", "Ant"+str(k)+ " dbg")
            plot_specific(df_ieee80211[df_ieee80211['phyNr'] == k],"OFDM-RESTART-ERROR", "Ant"+str(k)+ " dbg")
            plot_specific(df_ieee80211[df_ieee80211['phyNr'] == k],"ack_to", "Ant"+str(k)+ " dbg")

    #plt.show()
