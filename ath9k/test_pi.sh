INTERFACE="mon0"
phy="phy1"
IFNAME_TEST="wlp1s0"
IFNAME_ADMIN="wlan1"
DATA_DIR="/home/pi/20220514_rauen/0_10km"
TESTIDS=${2}

WIFI_NAME="drone"
WARM_UP_WIFI_TIME=10
IPERF_TIME=5
WORK_TIME=12
COOL_DOWN_TIME=2

SERVERIP=192.168.1.1
IPERF_DATARATE=100M
RTS_FRAG_THRESH=2346 ##  2346 disable rts / cts
TCPDUMP_SNAPLEN=126
#IPERF_PACKET_SIZE=1300

mode=${1}
mkdir -p ${DATA_DIR}
ifs=$(echo $INTERFACE | tr " " "\n")

ip link set "${IFNAME_ADMIN}" down 

# enforce tx status reports -> see outgoing frames
echo 1 > /sys/kernel/debug/ieee80211/"${phy}"/force_tx_status

iw dev "${IFNAME_TEST}" interface add mon0 type monitor && \
ip link set mon0 up &

if [ ${mode} == "client_s" ]; then
  TESTIDS=$TESTIDS_SINGLE
  mode="client"
fi

cp ${TESTIDS} ${DATA_DIR}

first=1
while IFS=, read -r testid clienttxpower bandwidth channel short_preamble retrys distance iperf_args
do
    read up rest </proc/uptime; start="${up%.*}${up#*.}"
    if [ ${first} == 1 ]; then # discard first csv lin
	      first=0
        continue
    fi
    echo "########################"
    echo "Starting test testid: ${testid} "
    echo "########################"

    ##setup interface
    iw reg set US && \
    ip link set "${IFNAME_TEST}" down && \
    echo ${bandwidth} > /sys/kernel/debug/ieee80211/"${phy}"/ath9k/chanbw && \
    iw phy "${phy}" set retry short ${retrys} long ${retrys} && \
    iw phy "${phy}" set frag "${RTS_FRAG_THRESH}" && \
    iw phy "${phy}" set rts "${RTS_FRAG_THRESH}" && \
    iw phy "${phy}" set distance "${distance}" && \
    ip link set "${IFNAME_TEST}" up && \
    sleep 1 && \
    iw dev "${IFNAME_TEST}" connect "${WIFI_NAME}" &

    sleep $((${WARM_UP_WIFI_TIME}))

    if [ ${mode} == "client" ]; then
      ip address add 192.168.1.2/24 dev "${IFNAME_TEST}" &
    fi

    #create tcpdump
    for if in ${ifs}
    do
      if [ ${mode} == "server" ]; then
        (tcpdump -i ${if} -w "${DATA_DIR}/${testid}-ieee80211/searchwing-tcpdump-${if}-${testid}.pcap" -s ${TCPDUMP_SNAPLEN}) & tcpdump_pid=$!
      else
        (tcpdump -i ${if} -w "${DATA_DIR}/${testid}-ieee80211/searchwing-tcpdump-${if}-${testid}.pcap" -s ${TCPDUMP_SNAPLEN}) & tcpdump_pid=$!
    fi
    done

    ATH9K_DEBUGFS='/sys/kernel/debug/ieee80211/'
    files=$(find ${ATH9K_DEBUGFS} -type f ! -iname '*eeprom*')
    for file in $files; do
            (mkdir -p $(dirname "${DATA_DIR}/${testid}-ieee80211/start/${file}") && \
              cp ${file} "${DATA_DIR}/${testid}-ieee80211/start/${file}") &
    done

    sleep $((${WARM_UP_AP_TIME}))

    #start benchmark
    echo "starting ${mode}"
    if [ ${mode} == "server" ]; then
      iperf3 -s --logfile ${DATA_DIR}/${testid}-ieee80211/iperf3-${testid}-server.txt &
      sleep ${WORK_TIME}
    else
      echo "starting client"
      (while true; do
        iperf3 -b ${IPERF_DATARATE} -c ${SERVERIP} ${iperf_args} -t ${IPERF_TIME} --repeating-payload --logfile ${DATA_DIR}/${testid}-ieee80211/iperf3-${testid}-client.txt # -l ${IPERF_PACKET_SIZE}
        sleep 1
      done) &

      # kill subshell process after IPERF_TIME
      pid_iperf=$!
      sleep ${WORK_TIME}
      kill ${pid_iperf}
    fi
    cat ${DATA_DIR}/${testid}-ieee80211/iperf3-${testid}-${mode}.txt

    killall iperf3 &
    killall tcpdump &

    files=$(find ${ATH9K_DEBUGFS} -type f ! -iname '*eeprom*')
    for file in $files; do
            (mkdir -p $(dirname "${DATA_DIR}/${testid}-ieee80211/end/${file}") && \
              cp ${file} "${DATA_DIR}/${testid}-ieee80211/end/${file}") &
    done

    #dump openwrt log

    sleep $((${COOL_DOWN_TIME}))

    read up rest </proc/uptime; end="${up%.*}${up#*.}"
    runtime=$((10*(${end}-${start})))
    echo "Runtime: ${runtime}"

done < ${TESTIDS}

ip link set "${IFNAME_TEST}" down
echo 20 > /sys/kernel/debug/ieee80211/"${phy}"/ath9k/chanbw
ip link set "${IFNAME_TEST}" up
iw dev "${IFNAME_TEST}" connect SW-AP1


ip link set "${IFNAME_ADMIN}" up