INTERFACE="mon0"
PHYS="phy0"
DATA_DIR="/tmp/20220514_rauen/0_10km"
TESTIDS=${2}

WIFI_NAME="drone"
WARM_UP_WIFI_TIME=10
IPERF_TIME=5
WORK_TIME=20
COOL_DOWN_TIME=2

SERVERIP=192.168.1.1
IPERF_DATARATE=100M
RTS_FRAG_THRESH=2346 ##  2346 disable rts / cts
TCPDUMP_SNAPLEN=126
#IPERF_PACKET_SIZE=1300

mode=${1}
mkdir -p ${DATA_DIR}
ifs=$(echo $INTERFACE | tr " " "\n")

# disable jit
sysctl net.core.bpf_jit_enable=0
# enforce tx status reports -> see outgoing frames
echo 1 > /sys/kernel/debug/ieee80211/phy0/force_tx_status

cp ${TESTIDS} ${DATA_DIR}

first=1
while IFS=, read -r testid clienttxpower bandwidth channel short_preamble retrys distance iperf_args
do
    read up rest </proc/uptime; start="${up%.*}${up#*.}"
    if [ ${first} == 1 ]; then # discard first csv lin
	      first=0
        continue
    fi
    echo "########################"
    echo "Starting test testid: ${testid} "
    echo "########################"

    ##setup interface
    uci set wireless.radio0.chanbw=${bandwidth}
    for phy in ${PHYS}
    do
      #iw phy ${phy} set retry short ${retrys} long ${retrys}
      iw phy ${phy} set rts ${RTS_FRAG_THRESH}
      iw phy ${phy} set frag ${RTS_FRAG_THRESH}
    done
    uci set wireless.radio0.distance=${distance}
    uci set wireless.radio0.short_preamble="${short_preamble}"
    uci set wireless.radio0.channel="${channel}"
    uci set wireless.radio0.rts="${RTS_FRAG_THRESH}"
    uci set wireless.radio0.frag="${RTS_FRAG_THRESH}"
    uci set wireless.radio0.log_level='0'
    if [ ${mode} == "client" ]; then
      uci set wireless.radio0.txpower="${clienttxpower}"
      uci set wireless.radio0.ssid="${WIFI_NAME}"
    fi
    if [ ${mode} == "server" ]; then
      uci set wireless.radio0.txpower="30"
      #uci set wireless.radio0.supported_rates=${datarate}
      #uci set wireless.radio0.basic_rate=${datarate}
      #uci set wireless.radio0.ht_capab=${ht_capab}
      uci set wireless.ap.ssid="${WIFI_NAME}"
      uci set wireless.ap.disassoc_low_ack='0'
      uci set wireless.ap.max_inactivity='3000'
    fi

    if [ ${mode} == "client" ]; then
      uci commit && wifi &
    else
      uci commit && wifi &
    fi

    sleep $((${WARM_UP_WIFI_TIME}))

    if [ ${mode} == "client" ]; then
      ip address add 192.168.1.2/24 dev sta
    fi

    #create tcpdump
    for if in ${ifs}
    do
      if [ ${mode} == "server" ]; then
        (tcpdump -i ${if} -w "${DATA_DIR}/${testid}-ieee80211/searchwing-tcpdump-${if}-${testid}.pcap" -s ${TCPDUMP_SNAPLEN}) & tcpdump_pid=$!
      else
        (tcpdump -i ${if} -w "${DATA_DIR}/${testid}-ieee80211/searchwing-tcpdump-${if}-${testid}.pcap" -s ${TCPDUMP_SNAPLEN}) & tcpdump_pid=$!
    fi
    done

    ATH9K_DEBUGFS='/sys/kernel/debug/ieee80211/'
    files=$(find ${ATH9K_DEBUGFS} -type f ! -iname '*eeprom*')
    for file in $files; do
            (mkdir -p $(dirname "${DATA_DIR}/${testid}-ieee80211/start/${file}") && \
              cp ${file} "${DATA_DIR}/${testid}-ieee80211/start/${file}") &
    done

    sleep $((${WARM_UP_AP_TIME}))

    #start benchmark
    echo "starting ${mode}"
    if [ ${mode} == "server" ]; then
      iperf3 -s --logfile ${DATA_DIR}/${testid}-ieee80211/iperf3-${testid}-server.txt &
      sleep ${WORK_TIME}
    else
      echo "starting client"
      (while true; do
        iperf3 -b ${IPERF_DATARATE} -c ${SERVERIP} ${iperf_args} -t ${IPERF_TIME} --repeating-payload --logfile ${DATA_DIR}/${testid}-ieee80211/iperf3-${testid}-client.txt # -l ${IPERF_PACKET_SIZE}
        sleep 1
      done) &

      # kill subshell process after IPERF_TIME
      pid_iperf=$!
      sleep ${WORK_TIME}
      kill ${pid_iperf}
    fi
    cat ${DATA_DIR}/${testid}-ieee80211/iperf3-${testid}-${mode}.txt

    killall iperf3 &
    killall tcpdump &

    files=$(find ${ATH9K_DEBUGFS} -type f ! -iname '*eeprom*')
    for file in $files; do
            (mkdir -p $(dirname "${DATA_DIR}/${testid}-ieee80211/end/${file}") && \
              cp ${file} "${DATA_DIR}/${testid}-ieee80211/end/${file}") &
    done

    #dump openwrt log
    logread > "${DATA_DIR}/${testid}-ieee80211/logreadOut" &

    sleep $((${COOL_DOWN_TIME}))
    killall logread &

    read up rest </proc/uptime; end="${up%.*}${up#*.}"
    runtime=$((10*(${end}-${start})))
    echo "Runtime: ${runtime}"

done < ${TESTIDS}

