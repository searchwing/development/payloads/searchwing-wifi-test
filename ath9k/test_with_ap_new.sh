
mode=${1}

SLEEPTIME=20
read up rest </proc/uptime; start="${up%.*}${up#*.}"

if [ ${mode} == "client" ] || [ ${mode} == "client_s" ]; then
	uci set wireless.sta.disabled=0 &&
        uci set wireless.ap.disabled=1 && wifi && sleep 5 &
fi
sleep ${SLEEPTIME}

read up rest </proc/uptime; end="${up%.*}${up#*.}"
runtime=$((10*(${end}-${start})))
echo "AP testmode waittime: ${runtime}"

./test.sh ${mode}

if [ ${mode} == "client" ] || [ ${mode} == "client_s" ]; then
	uci set wireless.radio0.chanbw=20
	uci set wireless.sta.disabled=1
        uci set wireless.ap.disabled=0 && uci commit && wifi &
fi

